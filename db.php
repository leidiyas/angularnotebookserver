<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET,PUT,POST,DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/**
        EasyDBManager TEST	
    */
		require_once './MySQLiManager/MySQLiManager.php';
        
        function insertar($text){
            $db = new MySQLiManager('localhost','root','','notebook');
            $data = array("text"=>"$text","status"=>0);
		    $db->insert('notas',$data);
        }
        
        function seleccionar(){
            $db = new MySQLiManager('localhost','root','','notebook');
            $notes = $db->select("*","notas");
            print(json_encode($notes,true));
        }

        function actualizarStatus($nId,$status){
            $db = new MySQLiManager('localhost','root','','notebook');
            $data = array("status"=>$status);
	        $db->update('notas',$data,"id = '$nId'");
        }

        if(isset($_GET["method"])){
            switch ($_GET["method"]) {
                case 'insertar':
                    if(!isset($_GET["text"])){
                        exit("El texto de la nota no fue definido");
                    }
                    insertar($_GET["text"]);
                    break;
                case 'actualizarStatus':
                    if(!isset($_GET["nid"]) || !($_GET["status"])){
                        exit("ERROR!");
                    }
                    actualizarStatus($_GET["nid"],$_GET["status"]);
                    break;
                default:
                    seleccionar();
                    break;
            }
        }
     